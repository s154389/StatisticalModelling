### 1. Assignment, 1. Theme: Wind
setwd("~/Documents/DTU/1. Semester (MSc)/Statistical Modelling/Assignments/Data")
library(MASS)

# 1. Read Data
D = read.table('tuno.txt', header = TRUE)

# 2. Descriptive Analysis
seq = 1:5000
seqN = seq(-15,15,0.1)
seqB = seq(0,1,0.001)
  # Power (red lines: Exp-fit, blue: Log-normal, green: gamma)
hist(D$pow.obs, breaks=20, freq = FALSE, ylim = c(0,0.0012))
lines(dexp(seq,rate = 1/mean(D$pow.obs)), col="red");
lines(dlnorm(seq, meanlog = mean(log(D$pow.obs)), sdlog=sd(log(D$pow.obs))), col="blue");
lines(dgamma(seq, shape = 0.8, rate = 1/mean(D$pow.obs)), col="green");


  # Scatter-matrix
plot(D)
plot(D[,c(2,4:6)])
    # Der ser ud til at være sammenhæng mellem:
    # Power som funktion af ws30 (Eksponentiel)
    # Power som funktion af wd30 (Svagt lineær)

  # Summary-statistics
summary(D)
    # Bemærk: Month går fra måned 1 til 10.

  # Power(month) =
boxplot(D$pow.obs ~ D$month)
    # Generelt lavere production i sensommeren end vinter/forår

  # Power(wd30) - kategoriseret i 0-1, 1-2, ... radianer= 
plot(D$wd30, D$pow.obs)
boxplot(D$pow.obs ~ floor(D$wd30))
    # Vi producerer umiddelbart højest ved en vinkel på ca. 4 radianer, svarende til
    # sydvestlig-retning. Lavest production ved nordlig retning. Mon ikke vindmøllen
    # vender
    # mod nord

  # Normalizing data
cap = 5000
D$pow.perc = D$pow.obs/cap # Power percentage of full capacity



hist(D$pow.perc,freq=FALSE)




############################################
##### Simple Models ######
# 1. Fit different pdf's to 3 variables
# I.e. treat response variable NOT as a function of other explaining variables (yet).

###############
# Power (red lines: Exp-fit, blue: Log-normal, green: gamma)
hist(D$pow.perc, breaks=20, freq = FALSE)
lines(seqB, dexp(seqB, rate = 1/mean(D$pow.perc)), col="red");
lines(seqB, dlnorm(seqB, meanlog = mean(log(D$pow.perc)), sdlog=sd(log(D$pow.perc))), col="blue");
lines(seqB, dgamma(seqB, shape = 0.8, rate = 1/mean(D$pow.perc)), col="green");
#hist(D$pow.obs, breaks=20, freq = FALSE, ylim = c(0,0.0012))
#lines(density(D$pow.obs), col = "red")
  ## EVT: OVER-DISPERSION TEST (GOODNEES OF FIT) AF VALGTE MODEL: GAMMA

boxcox(lm(D$pow.perc ~ 1), lambda = seq(0.2,0.5,0.001))
  # Choose lambda = 1/3

#### Do Box-Cox transformation of Power Percentage
      # Def. func.
y_lambda = function(y, lambda){
  if (lambda == 0){
    y_lambda = log(y)}
  else
    y_lambda = (y^lambda - 1) / lambda
}
      # Transform for lambda = 1/3
pp_bc = y_lambda(D$pow.perc, 1/3)
qqnorm(pp_bc)
qqline(pp_bc)
  # PowerPercentage is now approximately normal with large devaitions in both tails


#### Do y(lambda)-transformation as suggested in equation (1)
      # Kræver {y_i, lambda} > 0
y1_lambda = function(y, lambda){
    1 / lambda * log(y^lambda/(1-y^lambda))
}


  # BESTEM LIKELIHOOD AF Y SOM FUNKTION AF LAMBDA I N(µ,sigma)
  # First, change of variable: f_y (y) = f_ylambda(y_lambda) * g'(y)
logL_normalY1 = function(y,theta){ # theta = c(lambda, µ, sd)
  y_lambda = y1_lambda(y, theta[1])
  -( sum(dnorm(y_lambda, theta[2], theta[3], log=TRUE) + abs(log(-y*(-1 + y^theta[1])))) ) # VARIABELSKIFTE <=> tilføj g'(y)-led
}

(opt_pp = nlminb(c(1/3, 0.5, 0.5), logL_normalY1, y = D$pow.perc,
                 lower = c(0,-Inf,0), upper = c(Inf, Inf, Inf)))
  # Dvs. MLE: lambda = 0.2620769, µ = 2.5354303, sd = 4.6982469

  # Illustrér optimale parametre:
        # Y1-transformation af PowerPercentage med (optimal) lambda = 0.26
qqnorm(y1_lambda(D$pow.perc,opt_pp$par[1]))
qqline(y1_lambda(D$pow.perc,opt_pp$par[1]))

hist(y1_lambda(D$pow.perc, opt_pp$par[1]), breaks=20, freq = FALSE)
lines(seqN, dnorm(seqN, mean = opt_pp$par[2], sd = opt_pp$par[3]), col = "blue")


  # TIDLIGERE: LØST VED VISUEL INSPEKTION
qqnorm(y1_lambda(D$pow.perc,0.3))
qqline(y1_lambda(D$pow.perc,0.3))
    # lambda = 0.3 ser rigtig fornuftig ud (bedre end Box-Cox med lambda_opt = 1/3)

  

#### Do y(lambda)-transformation as suggested in equation (2)

y2_lambda = function(y, lambda){
  y_lambda = 2 * log(y^lambda / (1-y)^(1-lambda))
}
  # BESTEM LIKELIHOOD AF Y SOM FUNKTION AF LAMBDA I N(µ,sigma)
  # INDTIL DA, LØS VED VISUEL INSPEKTION (lambda = [0,1])
for (i in seq(0,1,0.1)){
  qqnorm(y2_lambda(D$pow.perc,i))
  qqline(y2_lambda(D$pow.perc,i))
}
    # lambda = 0.3 ser bedst ud, men afviger markent i halerne på meget lignende måde af Box-Cox
    # Et finere interval omkring 0.3 er undersøgt seq(0.2,0.5,0.033), men blev ikke bedre.3
    # Den bedste transformation er tydeligvis vha. eq. (1) med lambda_opt = 0.3 ???
    # (MANGLER: BESTEM LIKELIHOOD-FUNKTION AF PRODUKTET AF N(y1-transformeret-data),µ,sd))
    # -> BESTEM {µ, sd} (2D optimering)

    # Vi kan da fitte PowerPercentage ret godt med en normalfordeling:
D$Tpow.perc = y1_lambda(D$pow.perc,0.26)
hist(D$Tpow.perc, freq=FALSE, breaks = 25)
lines(seq(-15,15,0.1), dnorm(seq(-15,15,0.1), mean(D$Tpow.perc), sd(D$Tpow.perc)), col="red")


############### 
# Wind Speed (Evt. Fit GLM istedet for Gamma, hvis der er over/under dispersion)
hist(D$ws30, breaks=20, freq = FALSE)
lines(dgamma(seq, shape = 5, scale = 1.7), col = "blue")
  # Gamma fordeling ser fornuftig ud. Find optimale parametre {shape, scale} vha.
  # 2-parametre (log)Likelihood funktion:
logLgamma <- function(y, theta){
    -sum(dgamma(y, shape = theta[1], scale = theta[2], log = TRUE)) # LogLikelihood
}
(opt_ws = nlminb(c(1,1), logLgamma, lower=c(0,0), y = D$ws30))
  # Altså, MLE: {shape = 4.16, scale = 2.20}

# Illustrér optimale parametre:
hist(D$ws30, breaks=20, freq = FALSE)
lines(seq(0,25,0.1), dgamma(seq(0,25,0.1), shape = opt_ws$par[1], scale = opt_ws$par[2]), col = "blue")


###############
# Wind Direction - anvend Von Mises til cirkulær data
library(circular)
seqWD = seq(0,2*pi,0.1)

  # Fit von Mises distrubtion with parameters (mu, kappa) of decent fit
hist(D$wd30, freq = FALSE, breaks = 20)
lines(seqWD, dvonmises(seqWD, mu = 4.5, kappa = 0.7),col = "blue")

  # Find optimal parameters using (2-parameter) Likelihood
logLvm <- function(y, theta){
  -sum(dvonmises(y, mu = theta[1], kappa = theta[2], log = TRUE)) # LogLikelihood
}
(opt_wd = nlminb(c(5,1), logLvm, lower=c(0,0), y = D$wd30))
  # Altså, MLE: {µ = 4.70, kappa = 0.56}

  # Illustrér optimale parametre:
hist(D$wd30, breaks=20, freq = FALSE)
lines(seqWD, dvonmises(seqWD, mu = opt_wd$par[1], kappa = opt_wd$par[2]), col = "blue")
library(numDeriv)




###### 2. WALD CONFIDENCE INTERVALS AF PARAMETER
  # SD = sqrt( Hessian^-1 )

######
# Power - WALD CONFIDENCE INTERVALS
library(numDeriv)
H <- hessian(logL_normalY1, y=D$pow.perc, opt_pp$par)
se.beta <- sqrt(diag(solve(H)))

WALDSpower.perc = rbind(opt_pp$par, opt_pp$par - qnorm(1-0.05/2) * se.beta, opt_pp$par + qnorm(1-0.05/2) * se.beta)
colnames(WALDSpower.perc) = c("lambda", "µ", "sd")
rownames(WALDSpower.perc) = c("Estimate", "CI-lower", "CI-upper")
t(WALDSpower.perc)
  # Fair bit of uncertainty on the parameters

# Power - Likelihood based Confidence Intervals
seq_lambda = seq(0.01,1,0.01)
l_lambda_vals = c()
for (theta in seq_lambda){
  # Profile logLikelihood as a function of lambda
  l_lambda_vals = c(l_lambda_vals, logL_normalY1(D$pow.perc, c(theta, mean(y1_lambda(D$pow.perc,theta)), sd(y1_lambda(D$pow.perc,theta)))))
}
  # Illustrate CI's
plot(seq_lambda, -l_lambda_vals/max(-l_lambda_vals), 'l', ylim=c(0,1))
lines(seq_lambda, rep(exp(-1/2 * qchisq(1-0.05,1)), length(seq_lambda)), col=2)
    # HMMM... LOOKS BUGGED (MANGLER) OR all values of lambda between 0 and 1 are valid.

lp.p1(0.1,y)
p1 <- seq(0.01,0.99,by=0.01)
logLp1 <- sapply(p1,lp.p1,y=y)
logLp1 <- logLp1 - min(logLp1) ## normalization

plot(p1,exp(-logLp1),type="l")
lines(c(0,1), exp(-qchisq(0.95,df=1)/2)*c(1,1), col=2)

###
# Wind Speed
H_ws <- hessian(logLgamma, y=D$ws30, opt_ws$par)
se.beta_ws <- sqrt(diag(solve(H_ws)))
WALDSws = rbind(opt_ws$par, opt_ws$par - qnorm(1-0.05/2) * se.beta_ws, opt_ws$par + qnorm(1-0.05/2) * se.beta_ws)
  # Expected Value of Wind Speed:
µ_gamma = opt_ws$par[1] * opt_ws$par[2]
#Kontrol: mean(D$ws30)
    # var(µ) = var(shape * scale) = var(s)*var(sc) + var(s)*var(sc)^2 + var(s)^2 * var(sc) - ASSUMPTION: shape and scale are independant
sd_µ = sqrt( se.beta_ws[1]*se.beta_ws[2] + se.beta_ws[1]*se.beta_ws[2]^2 + se.beta_ws[1]^2*se.beta_ws[1])
  # WALD Confidence Interval for Expected value
WALDSws = cbind(WALDSws, c(µ_gamma, µ_gamma - qnorm(1-0.05/2)*sd_µ, µ_gamma + qnorm(1-0.05/2)*sd_µ))

colnames(WALDSws) = c("shape", "scale", "µ"); rownames(WALDSws) = c("Estimate", "CI-lower", "CI-upper")
t(WALDSws)

###
# Wind Direction - same process
H_wd <- hessian(logLvm, y=D$wd30, opt_wd$par)
se.beta_wd <- sqrt(diag(solve(H_wd)))
WALDSwd = rbind(opt_wd$par, opt_wd$par - qnorm(1-0.05/2) * se.beta_wd, opt_wd$par + qnorm(1-0.05/2) * se.beta_wd)
colnames(WALDSwd) = c("µ", "kappa")
rownames(WALDSwd) = c("Estimate", "CI-lower", "CI-upper")
t(WALDSwd)



# Only thing missing: Likelihood-baset CI for paramters for Wind D, Wind S
# and correct ones for Power Percentage